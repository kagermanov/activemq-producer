package com.narick.dbconnector;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
//import org.slf4j.Logger;
import org.apache.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class MSConnector {
    private static Connection conn;

    static String url = "jdbc:sqlserver://navsrv2:1433;databaseName=Replication";
    static String userName = "";
    static String password = "";
    static String server;
    static String dbname;

    static Logger logger = Logger.getLogger(MSConnector.class);

    public static Connection getConnection() {
        Properties properties = new Properties();
        try {
            properties = PropertiesLoaderUtils.loadProperties(new FileSystemResource("/deploy/config/dbconnection.properties"));
            //properties = PropertiesLoaderUtils.loadProperties(new FileSystemResource("./config/dbconnection.properties"));
            userName = properties.getProperty("jdbc.username");
            password = properties.getProperty("jdbc.password");
            server = properties.getProperty("jdbc.server");
            //dbname = properties.getProperty("jdbc.dbname");   // Replication
            dbname = properties.getProperty("jdbc.dbnameNew");  // ESBTrainingDumpDB

            if (conn == null) {
                SQLServerDataSource ds = new SQLServerDataSource();
                ds.setUser(userName);
                ds.setPassword(password);
                ds.setServerName(server);
                ds.setPortNumber(1433);
                ds.setDatabaseName(dbname);
                //ds.getSendStringParametersAsUnicode();
                return ds.getConnection();
            } else {
                return conn;
            }
        } catch (Exception sqlex) {
            sqlex.printStackTrace();
            logger.error("<<<<" + sqlex.getMessage() + ">>>>");
            throw new RuntimeException(sqlex.getMessage());
        }

    }

    public static boolean connectionClose() {
        try {
            if (conn != null) {
                conn.close();
                conn = null;
            }
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
            logger.error("<<<<" + sqlex.getMessage() + ">>>>");
            throw new RuntimeException(sqlex.getMessage());
        }
        return true;
    }

}
