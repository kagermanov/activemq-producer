package com.narick;

import javax.jms.JMSException;
import java.io.ByteArrayInputStream;

public class ApplicationTest {

    public static void main(String[] args) throws Exception {
        MyMessageSender sender = new MyMessageSender();

        String json = "{\"inquiryNo\": \"test72\", \"lineNo\": \"10000\", \"barcode\": \"7500135733512\", \"status\": \"4\", \"customerNo\": \"customer 01\", \"customerOrderNo\": \"cust order no 001\", \"orderStatus\": \"2\", \"eventDate\": \"2020-03-12 13:24:56\", \"addingDate\": \"2020-03-16 11:54:23\"}";
        sender.SendTopicMQ(new ByteArrayInputStream(json.getBytes()));
    }

}
