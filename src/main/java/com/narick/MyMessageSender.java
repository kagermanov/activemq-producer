package com.narick;

import com.google.gson.Gson;
import com.narick.dbconnector.MSConnector;

import javax.jms.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.sql.Connection;


import com.narick.model.Order;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;

import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;


@Path("message")
public class MyMessageSender {

    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private static final Charset ISO = Charset.forName("ISO-8859-1");

    static Logger logger = Logger.getLogger(MyMessageSender.class);
    static String subjectName = "Testqueue8";

    static String topicName = "TestTopic_1";


    // sending json message to topic
    // http://localhost:8181/cxf/queue/message/SendTopic
    @GET
    @Path("/SendTopic")
    public void SendTopicMQ(InputStream incomingData) throws Exception {
        String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        //ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        javax.jms.ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("karaf", "karaf", url);
        javax.jms.Connection connection = connectionFactory.createConnection();
        connection.start();

        // JMS messages are sent and received using a Session. We will
        // create here a non-transactional session object. If you want
        // to use transactions you should set the first parameter to 'true'
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Topic topic = session.createTopic(topicName);

        MessageProducer producer = session.createProducer(topic);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);  // не персистное означает что возможно доставка не будет произведена

        StringBuilder builder = new StringBuilder();
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null){
                builder.append(line);
            }
        }catch (Exception e) {
            System.out.println("Error Parsing: - ");
            logger.error("<<<<" + e.getMessage() + ">>>>");
        }

        TextMessage message = session.createTextMessage();

        message.setText(builder.toString());
        // sending the message
        producer.send(message);
        System.out.println("Sent message '" + message.getText() + "'");
        logger.info("<<< message sent: " + message.getText() + " >>>");

        connection.close();
    }


    // sending json message to queue
    // http://localhost:8181/cxf/queue/message/SendMessage
    @GET
    @Path("/SendMessage")
    public void SendMessageMQ(InputStream incomingData) throws JMSException {
        //URL of the JMS server. DEFAULT_BROKER_URL will just mean that JMS server is on localhost
        String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        System.out.println(url);

        // default broker URL is : tcp://localhost:61616"
        String subject = subjectName; // Queue/Topic Name.

        // Getting JMS connection from the server and starting it
        javax.jms.ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("karaf", "karaf", url);
        javax.jms.Connection connection = connectionFactory.createConnection();
        connection.start();

        System.out.println("--------------");

        //Creating a non transactional session to send/receive JMS message.
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        //The queue will be created automatically on the server.
        Destination destination = session.createQueue(subject);
        //Destination destination = session.createTopic(subject);

        // MessageProducer is used for sending messages to the queue.
        MessageProducer producer = session.createProducer(destination);

        StringBuilder builder = new StringBuilder();
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;
            while ((line = in.readLine()) != null){
                builder.append(line);
            }
        }catch (Exception e) {
            System.out.println("Error Parsing: - ");
            logger.error("<<<<" + e.getMessage() + ">>>>");
        }
        TextMessage message = session.createTextMessage(builder.toString());

        // sending message
        producer.send(message);

        System.out.println("message sent: '" + message.getText() + "'");
        connection.close();
    }


    // passed barcode uses to find a record in inquiry line and than send it to queue
    @GET
    @Path("/send/{barcode}")
    public void SendMessage(@PathParam("barcode") String barcode) throws JMSException {

        InputStream incomingData;

        String url = ActiveMQConnection.DEFAULT_BROKER_URL;
        System.out.println(url);

        // default broker URL is : tcp://localhost:61616"
        String subject = subjectName; // Queue/Topic Name

        // Getting JMS connection from the server and starting it
        javax.jms.ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("karaf", "karaf", url);
        javax.jms.Connection connection = connectionFactory.createConnection();
        connection.start();

        System.out.println("--------------");

        //Creating a non transactional session to send/receive JMS message.
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        //The queue will be created automatically on the server.
        Destination destination = session.createQueue(subject);

        // MessageProducer is used for sending messages to the queue.
        MessageProducer producer = session.createProducer(destination);

        Order order = GetOrderByBarcode(barcode);
        StringBuilder builder = new StringBuilder();
        // написать здесь получение json из объекта object
        // потом этот json передать в очередь
        //builder.append(Order)

        TextMessage message = session.createTextMessage(builder.toString());

        // message sending
        producer.send(message);

        System.out.println("message sent: '" + message.getText() + "'");
        connection.close();
    }


    private Order GetOrderByBarcode(String barcode) {
        Connection conn = null;

        String xmlResult = "";
        Order order = new Order();

        try {
            conn = MSConnector.getConnection();
            System.out.println("Connected to the database. Getting data.");

            String SQL = "select top 1 * from [REPLICATION_Логистика_2013$Inquiry Line] where [BarCode] = ? for xml auto;";
            PreparedStatement stmt = conn.prepareStatement(SQL);

            stmt.setString(1, barcode);

            try (ResultSet result = stmt.executeQuery()) {
                while (result.next()) {
                    order.setBarcode(result.getString("BarCode"));
                    order.setCustomerNo(result.getString("Customer No_"));
                    order.setCustomerOrderNo(result.getString("Customer Order No_"));
                    order.setInquiryNo(result.getString("Inquiry No_"));
                    order.setLineNo(result.getInt("Line No_"));
                    order.setOrderStatus(result.getInt("Order Status"));
                    order.setStatus(result.getInt("Status"));
                    //xmlResult += result.getString(1);
                }
            }

            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("<<<<" + e.getMessage() + ">>>>");
            return null;
        }

        MSConnector.connectionClose();

        return order;
    }

}
